pragma solidity ^0.4.16;

contract WellcomeContract {

  string public wellcomeString;

  function setWellcomeString(string newWellcomeString) {
      wellcomeString = newWellcomeString;
  }

}


pragma solidity ^0.4.18;

contract StringHolder {
    string savedString;

    function setString( string newString ) {
        savedString = newString;
    }

    function getString() constant returns( string ) {
        return savedString;
    }
}