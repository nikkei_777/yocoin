web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
abi = JSON.parse('[ { "constant": false, "inputs": [ { "name": "newString", "type": "string" } ], "name": "setString", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "getString", "outputs": [ { "name": "", "type": "string", "value": "Hello, world!" } ], "payable": false, "stateMutability": "view", "type": "function" } ]');
WellcomeContract = web3.eth.contract(abi);
contractInstance = WellcomeContract.at('0xf9E9898677c5A5568e892A3375Dc51D83BA1aAB1');

function updateWellcomeString() {
  let val = contractInstance.getString.call().toString();
  $("#wellcomeStringContainer").html(val);
}

function changeWellcomeString() {
  newString = $("#wellcomeStringInput").val();
  contractInstance.setString(newString, {from: web3.eth.accounts[0]}, function() {
    updateWellcomeString();
  });
}

$(document).ready(function() {
  updateWellcomeString();
});